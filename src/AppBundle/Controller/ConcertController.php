<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Concert;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class ConcertController extends Controller
{

    /**
     * @Route("/insertConcert", name="insertConcert")
     */
    public function insertConcertAction(Request $request)
    {
        $concert = new Concert();

        $form = $this->createFormBuilder($concert)
            ->add('name', TextType::class)
            ->add('author', TextType::class)
            ->add('date', DateType::class)
            ->add('city', TextType::class)
            ->add('space', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {        	
            $em = $this->getDoctrine()->getManager();
            $em->persist($concert);
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Concert inserted: '. $concert->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Insert Concert',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectConcert", name="selectConcert")
     */
    public function selectConcertAction(Request $request)
    {
        $concert = new Concert();

        $form = $this->createFormBuilder($concert)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $concerts = $this->getDoctrine()
                ->getRepository('AppBundle:Concert')
                ->findByName($concert->getName());
            if (count($concerts)==0) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No concert found for name '. $form->get('name')->getData()));
            }
            return $this->render('concert/content.html.twig', array(
                'concerts' => $concerts));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Select Concert',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/selectAllConcerts", name="selectAllConcerts")
     */
    public function selectAllConcertsAction()
    {
        $concerts = $this->getDoctrine()
            ->getRepository('AppBundle:Concert')
            ->findAll();

        if (count($concerts)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No concerts found'));
        }
        return $this->render('concert/content.html.twig', array(
            'concerts' => $concerts));
    }

    
    /**
     * @Route("/updateConcert", name="updateConcert")
     */
    public function updateConcertAction(Request $request)
    {
        $concert = new Concert();

        $form = $this->createFormBuilder($concert)
            ->add('name', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Select'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $concert = $em->getRepository('AppBundle:Concert')
                ->findOneByName($concert->getName());
            if (!$concert) {
                return $this->render('default/message.html.twig', array(
                    'message' => 'No concert found for name '. $form->get('name')->getData()));
            }
            return $this->redirectToRoute('editConcert',array('name' => $concert->getName()));

        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Concert',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/editConcert/{name}", name="editConcert")
     */
    public function editConcertAction($name, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $concert = $em->getRepository('AppBundle:Concert')
           ->findOneByName($name);

        $form = $this->createFormBuilder($concert)
            ->add('name', TextType::class)
            ->add('author', TextType::class)
            ->add('date', DateType::class)
            ->add('city', TextType::class)
            ->add('space', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Update'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            return $this->render('default/message.html.twig', array(
                'message' => 'Concert updated id: '. $concert->getId()
            ));
        }
        return $this->render('default/form.html.twig', array(
            'title' => 'Update Concert',
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/removeConcert", name="removeConcert")
     */
    public function removeConcertAction(Request $request)
    {
	
		$concert = new Concert();

      $form = $this->createFormBuilder($concert)
         ->add('name', TextType::class)
         ->add('save', SubmitType::class, array('label' => 'Remove'))
         ->getForm();

     	$form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
         $em = $this->getDoctrine()->getManager();
         $concert = $em->getRepository('AppBundle:Concert')
                      ->findOneByName($concert->getName());
								
         if (!$concert) {
             return $this->render('default/message.html.twig', array(
                 'message' => 'No concert found for name '. $form->get('name')->getData()));
         }
            
         $id = $concert->getId();
         $em->remove($concert);
         $em->flush();
         return $this->render('default/message.html.twig', array(
            'message' => 'Concert deleted: '. $id
         ));

		}
      
      return $this->render('default/form.html.twig', array(
			'title' => 'Remove Concert',
			'form' => $form->createView(),
		));
    }

    /**
     * @Route("/selectAllConcertsOrderedByName", name="selectAllConcertsOrderedByName")
     */
    public function selectAllConcertsOrderedByNameAction()
    {
        $concerts = $this->getDoctrine()
            ->getRepository('AppBundle:Concert')
            ->findAllOrderedByName();

        if (count($concerts)==0) {
            return $this->render('default/message.html.twig', array(
                'message' => 'No concert found'));
        }
        return $this->render('concert/content.html.twig', array(
            'concerts' => $concerts));
    }
    
}
