<?php

// src/AppBundle/Entity/Concert.php
namespace AppBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="concerts")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ConcertRepository")
 * @ORM\HasLifecycleCallbacks()
 */

class Concert
{
     /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $author;

    /**
     * @ORM\Column(type="date")
     */
    protected $date;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $city;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $space;

				// - GET ID
    /**
     * Get id
     * @return integer
     */
    public function getId(){
        return $this->id;
    }

				// - SET/GET NAME
    /**
     * Set name
     * @param string $name
     * @return Concerts
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }
    
    /**
     * Get name
     * @return string
     */
    public function getName(){
        return $this->name;
    }

				// - SET/GET AUTHOR
    /**
     * Set author
     * @param string $author
     * @return Concert
     */
    public function setAuthor($author){
        $this->author = $author;
        return $this;
    }

    /**
     * Get author
     * @return string
     */
    public function getAuthor(){
        return $this->author;
    }

				// - SET/GET DATE    
    /**
     * Set date
     * @param date $date
     * @return Concert
     */
    public function setDate($date){
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     * @return date
     */
    public function getDate(){
        return $this->date;
    }

				// - SET/GET CITY
    /**
     * Set city
     * @param string $city
     * @return Concert
     */
    public function setCity($city){
        $this->city = $city;
        return $this;
    }

    /**
     * Get city
     * @return string
     */
    public function getCity(){
        return $this->city;
    }
    
				// - SET/GET SPACE
    /**
     * Set space
     * @param string $space
     * @return Concert
     */
    public function setSpace($space){
        $this->space = $space;
        return $this;
    }

    /**
     * Get space
     * @return string
     */
    public function getSpace(){
        return $this->space;
    }

				// - ToSTRING
    /**
     * toString city
     * @return string
     */
    public function __toString (){
        return "(" . 
        		$this->id . ", " . 
        		$this->name . ", " .
        		$this->author . ", " . 
        		$this->date . ", " . 
        		$this->city . ", " .
        		$this->space . ")";
    }

    /**
     * Constructor
     */
    public function __construct(){}
  
}
