DROP DATABASE IF EXISTS dbconcerts;
CREATE DATABASE IF NOT EXISTS dbconcerts;
USE dbconcerts;
CREATE TABLE IF NOT EXISTS dbconcerts.concerts (
  id TINYINT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(30) UNIQUE,
  author VARCHAR(30),
  date DATE,
  city VARCHAR(30),
  space VARCHAR(50)
);

INSERT INTO concerts (name, author, date, city, space) VALUES
	('DOWNLOAD', 'HHSS', '2017-02-01', 'Londres', 'Espai juvenil londres'),
	('WACKEN', 'Anthrax', '2017-02-02', 'Berlín', 'Espai juvenil berlin'),
	('SPECIAL', 'Z-Z', '2017-02-03', 'Zimbabue', 'Espai juvenil zimbabue'),
	('nom', 'autor', '2017-02-02', 'ciutat', 'espai');
  
SELECT * FROM concerts;